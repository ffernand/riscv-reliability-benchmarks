//
// Created by fernando on 01/08/22.
//
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

void matrix_add(const short int *m1, const short int *m2, short int *m_out, uint32_t rows, uint32_t cols) {
    for (uint32_t i = 0; i < rows * cols; i++) {
        m_out[i] = m1[i] + m2[i];
    }
}

int main() {
    /**
     * Init input matrices as GAP8 benchmarks
     */
    uint32_t rows = 100;
    uint32_t cols = 100;

    printf("==================================\n");
    printf("Matrix size: %u x %u\n", rows, cols);
    printf("==================================\n");


    short int *M1 = (short int *) malloc(cols * rows * sizeof(short int));
    short int *M2 = (short int *) malloc(cols * rows * sizeof(short int));
    short int *Out1 = (short int *) malloc(cols * rows * sizeof(short int));
    if ((M1 == NULL) || (M2 == NULL) || (Out1 == NULL)) {
        printf("Failed to allocate memory for matrix.\n");
        return -1;
    } else {
        printf("Allocated matrix :\nM1 : %p, M2 : %p, \nOut1 : %p, %u\n",
               M1, M2, Out1, cols * rows);
    }

    for (int i = 0; i < rows * cols; i++) {
        M1[i] = 1;
        M2[i] = 2;
        Out1[i] = 0;
    }
    matrix_add(M1, M2, Out1, rows, cols);
    for (int i = 0; i < rows * cols; i++) {
        if (Out1[i] != 3) {
            printf("Out %hd\n", Out1[i]);
        }
    }
    free(M1);
    free(M2);
    free(Out1);

    return 0;
}