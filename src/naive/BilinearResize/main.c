/*
 * Copyright (C) 2017 GreenWaves Technologies
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD license.  See the LICENSE file for details.
 *
 * Adapted from https://github.com/GreenWaves-Technologies/gap_sdk
 * and here
 */



#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#define STACK_SIZE ( 1024 * 2 )
#define CID        ( 0 )

#define IMAGE_W    ( 322 )
#define IMAGE_H    ( 242 )

// Assume no image opening on simulation
#include "golden.h"

uint8_t *ImageOut;


void resize_image(uint32_t sourceWidth, uint32_t targetWidth, uint32_t sourceHeight,
                  uint32_t targetHeight, const uint8_t *input, uint8_t *output) {

    int a, b, c, d, x, y, index;
    float x_ratio = ((float) (sourceWidth - 1)) / (float) targetWidth;
    float y_ratio = ((float) (sourceHeight - 1)) / (float) targetHeight;
    float x_diff, y_diff, blue, red, green;
    int offset = 0;

    for (int i = 0; i < targetHeight; i++) {
        for (int j = 0; j < targetWidth; j++) {
            x = (int) (x_ratio * j);
            y = (int) (y_ratio * i);
            x_diff = (x_ratio * j) - x;
            y_diff = (y_ratio * i) - y;
            index = (y * (int) sourceWidth + x);
            a = input[index];
            b = input[index + 1];
            c = input[index + sourceWidth];
            d = input[index + sourceWidth + 1];

            // blue element
            blue = (a & 0xff) * (1 - x_diff) * (1 - y_diff) + (b & 0xff) * (x_diff) * (1 - y_diff) +
                   (c & 0xff) * (y_diff) * (1 - x_diff) + (d & 0xff) * (x_diff * y_diff);

            // green element
            green = ((a >> 8) & 0xff) * (1 - x_diff) * (1 - y_diff) + ((b >> 8) & 0xff) * (x_diff) * (1 - y_diff) +
                    ((c >> 8) & 0xff) * (y_diff) * (1 - x_diff) + ((d >> 8) & 0xff) * (x_diff * y_diff);

            // red element
            red = ((a >> 16) & 0xff) * (1 - x_diff) * (1 - y_diff) + ((b >> 16) & 0xff) * (x_diff) * (1 - y_diff) +
                  ((c >> 16) & 0xff) * (y_diff) * (1 - x_diff) + ((d >> 16) & 0xff) * (x_diff * y_diff);

            output[offset++] =
                    0x000000ff | // alpha
                    ((((int) red) << 24) & 0xff0000) |
                    ((((int) green) << 16) & 0xff00) |
                    ((((int) blue) << 8) & 0xff00);
        }
    }
}

int run_bilinear_Resize(void) {
    printf("Entering main controller\n");

    /* Input image size. */
    uint32_t w_in = (uint32_t) IMAGE_W, h_in = (uint32_t) IMAGE_H;
    /* Output image size, after resizing. */
    uint32_t w_out = w_in / 2, h_out = h_in / 2;

    ImageOut = (uint8_t *) malloc(w_out * h_out * sizeof(uint8_t));
    if (ImageOut == NULL) {
        printf("Failed to allocate memory for output image(%ld bytes).\n",
               w_out * h_out * sizeof(uint8_t));
        exit(-1);
    }

    /* Execute the function "cluster_main" on the Core 0 of cluster. */
    resize_image(w_in, w_out, h_in, h_out, ImageIn, ImageOut);
    printf("Close cluster after end of computation.\n");

    /* check output with golden for regressions. */
    int errors = 0;

    for (uint32_t i = 0; i < (w_out * h_out); i++) {
        if (ImageOut[i] != ImageOut_golden[i]) {
            errors++;
        }
    }

    printf("Test %s with %d error(s) !\n", (errors) ? "failed" : "success", errors);

    return errors;
}

int main() {
    printf("\n\n\t *** Bilinear Resize ***\n\n");
    return run_bilinear_Resize();
}
