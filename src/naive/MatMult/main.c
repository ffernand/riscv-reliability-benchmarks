//
// Created by fernando on 01/08/22.
//
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

void matrix_multiply(const short int *m1, const short int *m2, short int *m_out,
                     uint32_t rows_m1, uint32_t cols_m1, uint32_t rows_m2, uint32_t cols_m2, uint32_t cols_out) {
    for (uint32_t i = 0; i < rows_m1; i++) {
        for (uint32_t j = 0; j < cols_m2; j++) {
            short int sum = 0;
            for (uint32_t k = 0; k < rows_m2; k++) {
                sum = (short int) (sum + m1[i * cols_m1 + k] * m2[k * cols_m2 + j]);
            }
            m_out[i * cols_out + j] = sum;
        }
    }
}

int main() {
    /**
     * Init input matrices as GAP8 benchmarks
     */
    uint32_t rows_m1 = 200;
    uint32_t cols_m1 = 70;
    uint32_t rows_m2 = 70;
    uint32_t cols_m2 = 150;
    uint32_t rows_out = rows_m1;
    uint32_t cols_out = cols_m2;

    uint32_t num_op = rows_m1 * cols_m2 * (cols_m1 + rows_m2 - 1);

    printf("==================================\n");
    printf("Matrix 1: %u x %u\n", rows_m1, cols_m1);
    printf("Matrix 2: %u x %u\n", rows_m2, cols_m2);
    printf("Matrix out: %u x %u\n", rows_out, cols_out);

    printf("Matrix Multiplication number of operations: %u\n", num_op);
    printf("==================================\n");


    short int *M1 = (short int *) malloc(cols_m1 * rows_m1 * sizeof(short int));
    short int *M2 = (short int *) malloc(cols_m2 * rows_m2 * sizeof(short int));
    short int *Out1 = (short int *) malloc(cols_out * rows_out * sizeof(short int));
    if ((M1 == NULL) || (M2 == NULL) || (Out1 == NULL)) {
        printf("Failed to allocate memory for matrix.\n");
        return -1;
    } else {
        printf("Allocated matrix :\nM1 : %p, %u  M2 : %p, %u\nOut1 : %p, %u\n",
               M1, cols_m1 * rows_m1, M2, cols_m2 * rows_m2, Out1, cols_out * rows_out);
    }
    for (uint32_t i = 0; i < (cols_m1 * rows_m1); i++) {
        M1[i] = 1;
    }

    const short input[] = {2, 7, 10, 11, 13, 21, 24,
                           26, 28, 31, 43, 47, 51,
                           54, 57};

    for (uint32_t i = 0; i < (cols_m2 * rows_m2); i++) {
        uint32_t low = i & 0x0000000F;
        M2[i] = input[low];
    }
    //Set the 0 on the output
    for (uint32_t i = 0; i < (cols_out * rows_out); i++) {
        Out1[i] = 0;
    }

    matrix_multiply(M1, M2, Out1, rows_m1, cols_m1, rows_m2, cols_m2, cols_out);
    for (uint32_t i = 0; i < rows_out; i++) {
        for (uint32_t j = 0; j < cols_out; j++) {
            if(Out1[i * cols_out + j] >= 32767 || Out1[i * cols_out + j] < 0){
                printf("Overflow %hd\n", Out1[i * cols_out + j]);
            }
        }
    }

    free(M1);
    free(M2);
    free(Out1);

    return 0;
}